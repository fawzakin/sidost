#!/usr/bin/env bash
# Simple Dotfile Shelver (sido.sh)
# Quickly deploy dotfiles and programs with simple, efficient, and easy-to-understand script
# Made for Arch Linux and Debian
# Author: Fawzakin
# Repository: https://gitlab.com/fawzakin/sidosh
# License: GNU GPL v3 

#======================#
#||  CONFIGURATIONS  ||#
#======================#

# Main configurations
# See information about each variable at the gitlab page on the top of this script
name="$(logname)"
home_dir="/home/${name}"
aur_helper="paru"
source_dir="${home_dir}/src"
dotfiles_dir="${home_dir}/.dotfiles"
dotfiles_repo="https://gitlab.com/fawzakin/dotfiles.git"
dotfiles_branch=""
prog_pac="list_pac.csv"
prog_deb="list_deb.csv"

# List of fonts to install. It's recommended to only get your font this way 
# if the font you need is not in both Debian and Arch repository.
# otherwise, you should put them in your program list if possible.
declare -A font_url=(
[fira]="https://www.1001fonts.com/download/fira-sans.zip"
[jetbrains]="https://github.com/ryanoasis/nerd-fonts/releases/download/v3.0.2/JetBrainsMono.zip"
[iosevka]="https://github.com/be5invis/Iosevka/releases/download/v28.1.0/PkgTTF-IosevkaSS14-28.1.0.zip"
)

# List of theme and icon to install.
# Again, it's best to get them from program list if both repo have them
declare -A theme_url=(
[theme]="https://github.com/dracula/gtk/archive/master.zip"
[icon]="https://github.com/dracula/gtk/files/5214870/Dracula.zip"
)

# Main functions to run. The order is top first bottom last.
# Comment one of the line below to disable them.
declare -a functions_run=(
"pkgdeb"
"pkgpac"
"pkgaur"
"gitmake"
"dotinstall"
"fontinstall"
"themeinstall"
"postinstall"
)

#======================#
#||  MAIN FUNCTIONS  ||#
#======================#

# Do NOT modify anythingujian.ui.ac.id beyond this part unless you want to modify the program itself
# LEGEND for echo messages: [*] Information, [!] Warning/Error, [!!] Fatal Error (exit the script)

pkgdeb() {
    [ -x "/usr/bin/apt" ] || return 1
    [ "$(wc -l < /tmp/list-deb)" -eq 0 ] && echo "[!] List for debian packages is empty. Skipping." && return 0
    echo "[*] Updating mirrors and installing packages. This will take a while."
    { apt update -y && apt upgrade -y && xargs apt install -y < /tmp/list-deb; } ||
    errormsg "[!!] Failed to install packages.\\n     If it's network error, rerun this script.\\n     If any package is not found, configure you source.list to allow your program to be retrieved from debian's specific source and recheck your list."
}

pkgpac() { 
    [ -x "/usr/bin/pacman" ] || return 1
    [ "$(wc -l < /tmp/list-pac)" -eq 0 ] && echo "[!] List for pacman packages is empty. Skipping." && return 0
    echo "[*] Configuring mirror and installing packages. This will take a while."
    pacman -S --noconfirm archlinux-keyring > /dev/null 2>&1 # Refreshing keyrings
    pacman -S --noconfirm --needed reflector > /dev/null 2>&1 &&
    reflector -l 6 --protocol https --sort rate --save /etc/pacman.d/mirrorlist > /dev/null 2>&1 ||
    echo "[!] Failed to install/executing reflector. Continuing!"
    pacman -Syyu --noconfirm --needed - < /tmp/list-pac ||
    errormsg "[!!] Failed to install packages. If it's 'failed retrieving file' error, rerun this script. If any package is not found, recheck your list."
}

pkgaur() {
    [ -x "/usr/bin/pacman" ] || return 1
    [ "$(wc -l < /tmp/list-aur)" -eq 0 ] && echo "[!] List for AUR packages is empty. Skipping." && return 0
    echo "[*] Installing AUR packages. This will take a while."
    [ -x "$(command -v "${aur_helper}")" ] || aurinstall
    sed -i "s/-j2/-j$(nproc)/;s/^#MAKEFLAGS/MAKEFLAGS/" /etc/makepkg.conf # Optimize package compiling
    # shellcheck disable=SC2024
    sudo -u "${name}" "${aur_helper}" -S --noconfirm --needed - < /tmp/list-aur ||
    errormsg "[!!] Failed to install packages.\\n     If it's network error, rerun this script.\\n     If it's package not found, check your list."
}

gitmake() {
    [ "$(wc -l < /tmp/list-git)" -eq 0 ] && echo "[*] List for git programs is empty. Skipping." && return 0
    { [ -x "$(command -v git)" ] && [ -x "$(command -v make)" ]; } || errormsg "[!!] git and/or make aren't installed."
    echo "[*] Installing programs from git with 'make install'."

    while read -r repo ; do # Iterate every git repository 
        repo_name="$(urltrim "${repo%.git}")"
        repo_loc="${source_dir}/${repo_name}"
        echo "[*] Installing ${repo_name:-${repo}}."
        [ -d "${repo_loc}" ] || sudo -u "${name}" git clone "${repo}" --depth 1 "${repo_loc}" > /dev/null 2>&1 ||
	{ echo "[!] Failed to clone ${repo}."; continue; }
        cd "${repo_loc}" || return 1
        make install > "${repo_name}.log" 2>&1 || echo "[!] Error installing ${repo_name}."
    done < /tmp/list-git
}

dotinstall() { 
    { [ -x "$(command -v git)" ] && [ -x "$(command -v stow)" ]; } || errormsg "[!!] git and/or stow aren't installed."
    echo "[*] Installing dotfiles repo for ${name}."

    # Check if dotfiles_dir is a populated folder. Clone if it is empty or does not exist
    { [ ! -d "${dotfiles_dir}" ] ||  [ -z "$(ls -A ${dotfiles_dir})" ]; } && do_clone=1 
    [ -z "${do_clone}" ] || sudo -u "${name}" git clone "${dotfiles_repo}" --depth 1 "${dotfiles_dir}" > /dev/null 2>&1 ||
    errormsg "[!!] Failed to clone the dotfiles repo."
    cd "${dotfiles_dir}" || return 1

    # Change branch if dotfiles_branch is not empty
    [ -z "${dotfiles_branch}" ] || sudo -u "${name}" git checkout "${dotfiles_branch}" > /dev/null 2>&1 || 
    echo "[!] ${dotfiles_branch} branch does not exist or ${dotfiles_dir} is not a git repo. Continuing with the default branch."

    # Installing the configs with stow
    for dir in */; do 
        sudo -u "${name}" stow --no-folding -t "${home_dir}" "${dir}" > /dev/null 2>&1 ||
        echo "${dir}" >> "${home_dir}"/.stow-conflict
    done 
    chown "${name}":"${name}" "${home_dir}/.stow-conflict" > /dev/null 2>&1 || no_conflict=1
    echo "[*] Dotfiles has been stowed to ${name}'s home directory"
    [ -z "${no_conflict}" ] && printf "%s\\n%s\\n%s\\n" \
        "[!] You have conflicting configs that need to be solved manually!" \
	"    You can do that by removing or renaming those conflicting configs in '~/.config' or '~/.local'." \
	"    See ~/.stow-conflict to see the list of conflicting configs."
}

fontinstall() {
    [ "${#font_url[@]}" -eq 0 ] && echo "[!] List for font is empty. Skipping." && return 0
    for f in "${!font_url[@]}"; do 
        echo "[*] Downloading ${f} font from ${font_url[${f}]}."
        curlunzip "${font_url[${f}]}" "/usr/share/fonts/${f}" || echo "[!] Failed to install ${f} font. Continuing."
    done
}

themeinstall() {
    [ "${#theme_url[@]}" -eq 0 ] && echo "[!] List for theme and icon is empty. Skipping." && return 0
    echo "[*] Downloading theme and icon."
    theme_loc="/usr/share/themes/"
    icon_loc="/usr/share/icons/"
    [ -z "${theme_url["theme"]}" ] || curlunzip "${theme_url["theme"]}" "${theme_loc}" || echo "[!] Failed to install theme. Continuing."
    [ -z "${theme_url["icon"]}" ] || curlunzip "${theme_url["icon"]}" "${icon_loc}" || echo "[!] Failed to install icon. Continuing."
}

# The following function will run configurations for certain programs when installed
postinstall() {
    echo "[*] Finishing the setup."

    # Add the user to video to allow changing brightness
    echo "[*] Adding ${name} to 'video' group."
    grep -q "video" /etc/group || groupadd video; usermod -aG video "${name}"
    mkdir -p /etc/udev/rules.d
    #shellcheck disable=SC2016
    echo 'ACTION=="add", SUBSYSTEM=="backlight", RUN+="/bin/chgrp video $sys$devpath/brightness", RUN+="/bin/chmod g+w $sys$devpath/brightness"' > /etc/udev/rules.d/backlight.rules

    # Configuration for zsh users
    echo "[*] Installing plugins for zsh."
    if [ -x "$(command -v zsh)" ]; then
        sudo -u "${name}" mkdir -p "${home_dir}/.config/zsh_plugins"
        sudo -u "${name}" git clone https://github.com/zsh-users/zsh-autosuggestions.git              "${home_dir}/.config/zsh_plugins/zsh-autosuggestions"      
        sudo -u "${name}" git clone https://github.com/zdharma-continuum/fast-syntax-highlighting.git "${home_dir}/.config/zsh_plugins/fast-syntax-highlighting"
        usermod --shell /bin/zsh "${name}" # Change default shell to zsh
    fi

    # Installing Dracula Kvantum themes if the base Dracula GTK theme is installed
    #shellcheck disable=SC2086
    theme_loc="/usr/share/themes/"
    [ -d "${theme_loc}/gtk-master" ] && \
        mkdir -p "/usr/share/Kvantum/" && \
        cp -rf ${theme_loc}/gtk-master/kde/kvantum/* "/usr/share/Kvantum/"
        mv -f "${theme_loc}/gtk-master" "${theme_loc}/Dracula"
}

#=====================#
#||  SUB FUNCTIONS  ||#
#=====================#

errormsg() { printf "%s\\n" "$1" 1>&2; exit 1; } # Print error messages to stderr and exit

urltrim() { echo "${1}" | rev | cut -d"/" -f 1 | rev; }

curlunzip() { # Download file from ${1} and extract them to ${2}
    f="${source_dir}/$(urltrim "${1}")";
    [ -f "${f}" ] || curl -L "${1}" -o "${f}"
    mkdir -p "${2}" && unzip -n "${f}" -d "${2}" 
}

getlist() { # Extracting prog_list to separate lists
    echo "[*] Getting the program list."
    [ -z "$1" ] && true > /tmp/list.csv && return 0
    [[ "$1" =~ http://|https:// ]] && { curl -sL "$1" -o /tmp/list.csv || errormsg "[!!] The link for program list is invalid."; } && return 0
    [ -f "$1" ] && { cp -f "$1" /tmp/list.csv || errormsg "[!!] Program list not found."; } && return 0
}

aurinstall() { # Installing preferred AUR helper if you don't have it already. It install the -bin version if it exists
    echo "[*] Installing ${aur_helper}."
    aur_loc="${source_dir}/${aur_helper}"
    [ "$(git ls-remote "https://aur.archlinux.org/${aur_helper}-bin.git" | wc -l)" -ne 0 ] && aur_affix="-bin"
    [ -d "${aur_loc}" ] || sudo -u "${name}" git clone "https://aur.archlinux.org/${aur_helper}${aur_affix}.git" "${aur_loc}" > /dev/null 2>&1
    cd "${aur_loc}" && sudo -u "${name}" makepkg -si --noconfirm > /dev/null 2>&1
}

#====================#
#||  MAIN PROGRAM  ||#
#====================#

# Getting the options
while getopts ":t:a:d:r:b:p:m:h" op; do case "${op}" in
    t) [ -z "${OPTARG}" ] || { name="${OPTARG}"; home_dir="/home/${name}"; dotfiles_dir="${home_dir}/.dotfiles"; } ;;
    a) [ -z "${OPTARG}" ] || aur_helper="${OPTARG}" ;;
    d) [ -z "${OPTARG}" ] || dotfiles_dir="${OPTARG}" ;;
    r) dotfiles_repo="${OPTARG}" ;;
    b) dotfiles_branch="${OPTARG}" ;;
    p) prog_list="${OPTARG}" ;;
    h) printf "Available Options:\\n%s\\n%s\\n%s\\n%s\\n%s\\n%s\\n%s\\n\\n%s\\n%s\\n" \
           "  -t: Target user" \
           "  -a: Preferred AUR helper (must have pacman-like syntax)" \
           "  -d: Path for dotfiles repository. Always put this option after -t option" \
           "  -r: Dotfiles repository URL. Can be empty if -d is already a dotfiles folder/repo" \
           "  -b: Dotfiles repository branch. Can be empty if using default branch" \
           "  -p: List of programs in csv (local file or url). Make sure the list contains packages for your distro." \
           "  -h: Show this message and exit" \
           "For more information about each option, take a look at this script with a text editor or at the README here:" \
           "https://gitlab.com/fawzakin/sido.sh"
       exit 0 ;;
    *) printf "Invalid option: -%s\\nUse -h to see available options\\n" "${OPTARG}" && exit 1 ;;
esac done

# Error handling. This will quit the script whenever this function called to prevent any potential damage to system
{ [ -x "/usr/bin/pacman" ] && prog_list="${prog_pac}"; } ||
{ [ -x "/usr/bin/apt" ]    && prog_list="${prog_deb}"; } || 
                         errormsg "[!!] You are not running on Debian or Arch (derivative distros like Devuan and Artix may not work as intended)."
[ "$(id -u)" -ne 0 ]  && errormsg "[!!] Please run this script as root (sudo ./sido.sh.sh)."
[ "${name}" = root ]  && errormsg "[!!] Please make sure that the target user is not root (./sido.sh.sh -t USERNAME)."
[ -z "${name}" ]      && errormsg "[!!] Target user cannot be empty."
[ -d "${home_dir}" ] || errormsg "[!!] Target user ${name} does not exist."

# Extracting list from prog_list
for list in list-aur list-deb list-git list-pac; do true > "/tmp/${list}"; done
getlist "${prog_list}"
while IFS=, read -r tag program _; do case ${tag} in
    a|A) echo "${program}" >> /tmp/list-aur ;;
    d|D) echo "${program}" >> /tmp/list-deb ;;
    g|G) echo "${program}" >> /tmp/list-git ;;
    p|P) echo "${program}" >> /tmp/list-pac ;;
esac done < /tmp/list.csv

# Setting up our source path for git and other downloaded content.
sudo -u "${name}" mkdir -p "${source_dir}"

# And finally, run our main functions
for run in "${functions_run[@]}"; do ${run}; done
chown -R "${name}":"${name}" "${source_dir}"
printf "[*] sido.sh has finished!\\n"
