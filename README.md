![200 IQ Graphic Design](https://gitlab.com/fawzakin/sidost/raw/main/sidost.png)

# Simple Dotfiles Installer (SIDOST)
SIDOST is a small bash script to quickly deploy dotfiles, install necessary programs (like a window manager), and other system configuration for a fresh install of Arch. By default, it will use [my dotfiles repo](https://gitlab.com/fawzakin/dotfiles) and list of programs to install a minimal, working and usable desktop environment with my build of [dwm](https://gitlab.com/fawzakin/dwm). Works best if Arch is installed using my [Easy Arch script](https://gitlab.com/fawzakin/easy-arch).

This script only supports Arch Linux. While base Arch installation is recommended, it should be compatible with other Arch-based distro (e.g. Arco, Artix, Endeavour, Garuda, Manjaro, and the new SteamOS). ~~[Debian](https://gitlab.com/fawzakin/sidost/tree/deb) and Fedora version can be found in their respective branches~~ Debian and Fedora versions are cancelled due to lack of use between the two.

The goal of this project is to create a desktop installer that is easy to understand, easy to maintain, and very modular. It is much simpler and efficient than my previous [dotfiles installers](https://gitlab.com/fawzakin/dotfiles-old) which are overly-complicated, hard to maintain, and just annoying to work with in general.

# Installation
Download my script and run it as root from a normal user.
```
curl -LO tiny.one/sidost
sudo bash sidost
```
If you don't have a normal account beforehand and you are logged in as root,
run the following commands to create a normal account and then use -t option to set it as the target user:
```
useradd -m [username] # Make a normal account with [username] as name and its home directory.
bash sidost -t [username] # Run the script for [username]
usermod -aG wheel [username] # Give your new [username] sudo priveledges. Optional.
passwd [username] # Give your new [username] a password. Recommended.
```
If there's network error when installing packages with pacman, just run this script again.

After the script has finished the installation, logout and login into your target account to enjoy your new desktop.

# Default Config
With the default program list and dotfiles repo provided in the script, you'll get the following:
- My build of dwm plus dmenu, st, my fork of dmscripts, and my custom gtk/qt theme
- Minimalist programs for basic stuff such as thunar, mpv, zathura, sxiv, etc. (configured and ready to use)
- Papirus icons, KDE Breeze cursor, and a simple Arch wallpaper

Make sure to run `post-install` after logging into your new desktop (press mod+enter to open the terminal). It is only required if you deploy my dotfiles repo.

If you are using the default config for Manjaro or Artix, some packages listed in `list.csv` may not exist in the repository of each distro since they are separated from the main Arch repository. You can enable Arch repository in Artix by following the steps [here](https://wiki.artixlinux.org/Main/Repositories).

If you install the default config on a virtual machine, you can change the resolution by running `xrandr -s [width]x[height]` or by editing autostart (press mod+minus and search for 'autostart'). It is generally recommended to set your VM to the proper resolution before running it.

# Configurations Variables
Here are the variables you can modify for the usage of your own dotfiles repository and program list. You can also use options when running the script as explained below.

**name/home_name [-t NAME]**: The target username and its home directory. By default, it will target the user from the output of `logname` (the currently logged in user).

**aur_helper [-a AUR]**: AUR helper you want to use. If you don't have it installed beforehand, the script will automatically install it for you (it will install the -bin version if it exists). Make sure it has pacman-like syntax (with "-S" option and whatnot).

**dotfiles_dir [-d PATH]**: The directory of your dotfiles. This will be the place where your dotfiles repo set in `dotfiles_repo` will be cloned into when it is either empty or does not exist at first. If it is already an occupied folder, the script will not clone the said repo. This is useful if you don't want to host a git repository on a git-hosting website. It must be in a form of a "stow directory" explained in the next variable. By default, it will be located in `~/.dotfiles`.

NOTE: Always put -d after -t. Otherwise, -d will get overriden by -t where it sets the dir to `TARGET-HOME/.dotfiles`.

**dotfiles_repo [-r URL]**: Your dotfiles git repository. It is must be in a form of a "stow directory." It is quite hard to explain but these blogs by [Alex Pearce](https://alexpearce.me/2016/02/managing-dotfiles-with-stow/) and [Steven R. Baker](https://www.stevenrbaker.com/tech/managing-dotfiles-with-gnu-stow.html) are good places to start understanding how GNU stow works.
By default, it will use my repository which can be used an example of a stow package repository.

**dotfiles_branch [-b BRANCH]**: The branch of your dotfiles repository you want to use. By default, it is empty so that git can pull the default branch.

**prog_list [-p PATH/URL]**: The list of programs, dependencies for git programs, and git repo you want to install in two columns csv file. The list file can be from a local path or a raw text file retrievable from the internet with `curl`.

The format of the csv list is "TAG,PROGRAM" where the "TAG" is for how the program will be installed and "PROGRAM" is the program name itself. The TAGs are `P` for pacman, `A` for AUR, and `G` for git. For git, the program column must be in an http URL that ends with ".git". Make sure that every program installed from git can be installed with simple `make install`. The git repo will be cloned into `TARGET-HOME/.src`.

In order for the whole script to work properly, you need to include the following into your list (all uses `P` TAG):
- base-devel
- git
- stow

**function_run**: Main functions to run. You can comment some of it to skip parts of the script. This is useful if you want to skip, let's say, the package parts and just want to install your dotfiles for other users.

# Main Functions

`pkgpacman`: Install Arch packages. It will start by refreshing the keyrings and mirrorlist with `reflector`.

`pkgaur`: Install AUR packages. It will install the preferred AUR helper if you don't have it already.

`gitmake`: Install programs from git repo with `make install`.

`dotinstall`: Install your dotfiles with `stow`. If there's any conflict, `.stow-conflict` will appear at your homedir so you can remove or rename the conflicting configs manually.

`postinstall`: Finalization of this script. This will run any required configuration for certain program when they are installed such as `zsh` changing target user's login shell.

# License and Credit:
GPLv3 License. Big thanks to Luke Smith's [LARBS](https://larbs.xyz) as an inspiration. While I wrote everything from scratch, some ideas were shared from his script.
